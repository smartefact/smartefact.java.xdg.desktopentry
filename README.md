[![Latest release](https://gitlab.com/smartefact/smartefact.java.xdg.desktopentry/-/badges/release.svg?key_text=Latest%20release&key_width=100)](https://gitlab.com/smartefact/smartefact.java.xdg.desktopentry/-/releases)
[![Pipeline status](https://gitlab.com/smartefact/smartefact.java.xdg.desktopentry/badges/main/pipeline.svg?key_text=Pipeline%20status&key_width=100)](https://gitlab.com/smartefact/smartefact.java.xdg.desktopentry/-/commits/main)
[![Coverage](https://gitlab.com/smartefact/smartefact.java.xdg.desktopentry/badges/main/coverage.svg?key_text=Coverage&key_width=100)](https://gitlab.com/smartefact/smartefact.java.xdg.desktopentry/-/commits/main)

# smartefact.java.xdg.desktopentry

Java utilities for the [XDG Desktop Entry specification](https://www.freedesktop.org/wiki/Specifications/desktop-entry-spec/).

[Javadoc](https://smartefact.gitlab.io/smartefact.java.xdg.desktopentry)

## Changelog

See [CHANGELOG](CHANGELOG.md).

## Contributing

See [CONTRIBUTING](CONTRIBUTING.md).

## License

This project is licensed under the [Apache License 2.0](LICENSE).
