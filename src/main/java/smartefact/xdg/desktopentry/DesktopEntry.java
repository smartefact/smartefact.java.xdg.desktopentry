/*
 * Copyright 2023 Smartefact
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

package smartefact.xdg.desktopentry;

import java.io.IOException;
import java.io.InputStream;
import java.io.OutputStream;
import java.io.Reader;
import java.io.Writer;
import java.util.ArrayList;
import static java.util.Collections.unmodifiableMap;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.regex.Matcher;
import java.util.regex.Pattern;
import org.jetbrains.annotations.Contract;
import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import smartefact.Characters;
import smartefact.Collections;
import static smartefact.HashCodeBuilder.hashCodeBuilder;
import smartefact.InputStreams;
import smartefact.OutputStreams;
import static smartefact.Preconditions.require;
import static smartefact.Preconditions.requireNotNull;
import smartefact.Strings;
import static smartefact.ToStringBuilder.toStringBuilder;

/**
 * XDG desktop entry.
 * <p>
 * <b>Specification:</b> XDG Desktop Entry 1.5
 * </p>
 *
 * @author Laurent Pireyn
 * @see <a href="https://specifications.freedesktop.org/desktop-entry-spec/desktop-entry-spec-latest.html">XDG Desktop Entry Specification</a>
 */
public final class DesktopEntry {
    /**
     * Line in a desktop entry.
     * <p>
     * This is a sealed class.
     * </p>
     *
     * @see Entry
     * @see Comment
     * @see BlankLine
     */
    public abstract static class Line {
        Line() {}

        abstract void writeTo(@NotNull Writer writer) throws IOException;

        @Override
        @Contract(value = "null -> false", pure = true)
        public abstract boolean equals(@Nullable Object object);

        @Override
        @Contract(pure = true)
        public abstract int hashCode();

        @Override
        @Contract(pure = true)
        public abstract @NotNull String toString();
    }

    /**
     * Entry {@link Line} in a desktop entry.
     */
    public static final class Entry extends Line {
        public static boolean isLegalKeyChar(char c) {
            return Characters.in(c, 'a', 'z')
                || Characters.in(c, 'A', 'Z')
                || Characters.in(c, '0', '9')
                || c == '-';
        }

        public static boolean isLegalKey(@NotNull String string) {
            requireNotNull(string);
            return !string.isEmpty()
                && Strings.all(string, Entry::isLegalKeyChar);
        }

        private final @NotNull String key;
        private final @NotNull String value;

        public Entry(
            @NotNull String key,
            @NotNull String value
        ) {
            require(
                isLegalKey(key),
                () -> "The string <" + key + "> is an illegal key"
            );
            requireNotNull(value);
            this.key = key;
            this.value = value;
        }

        @Contract(pure = true)
        public @NotNull String getKey() {
            return key;
        }

        @Contract(pure = true)
        public @NotNull String getValue() {
            return value;
        }

        @Contract(pure = true)
        public @Nullable Boolean getValueAsBooleanOrNull() {
            switch (value) {
                case "true":
                    return Boolean.TRUE;
                case "false":
                    return Boolean.FALSE;
                default:
                    return null;
            }
        }

        @Contract(pure = true)
        public @Nullable Double getValueAsNumericOrNull() {
            return Strings.toDoubleOrNull(value);
        }

        @Override
        void writeTo(@NotNull Writer writer) throws IOException {
            writer.write(key);
            writer.write(" = ");
            for (int i = 0; i < value.length(); ++i) {
                char c = value.charAt(i);
                boolean escape = false;
                switch (c) {
                    case ' ':
                        // Escaping the spaces is ugly, but it is necessary to escape leading and trailing spaces
                        escape = true;
                        c = 's';
                        break;
                    case '\n':
                        escape = true;
                        c = 'n';
                        break;
                    case '\t':
                        escape = true;
                        c = 't';
                        break;
                    case '\r':
                        escape = true;
                        c = 'r';
                        break;
                    case '\\':
                        escape = true;
                        c = '\\';
                        break;
                }
                if (escape) {
                    writer.write('\\');
                }
                writer.write(c);
            }
            writeEolTo(writer);
        }

        @Override
        @Contract(value = "null -> false", pure = true)
        public boolean equals(@Nullable Object object) {
            if (this == object) {
                return true;
            }
            if (!(object instanceof Entry)) {
                return false;
            }
            final @NotNull Entry other = (Entry) object;
            return key.equals(other.key)
                && value.equals(other.value);
        }

        @Override
        @Contract(pure = true)
        public int hashCode() {
            return hashCodeBuilder()
                .property(key)
                .property(value)
                .build();
        }

        @Override
        @Contract(pure = true)
        public @NotNull String toString() {
            return toStringBuilder(this)
                .property("key", key)
                .property("value", value)
                .build();
        }
    }

    /**
     * Comment {@link Line} in a desktop entry.
     */
    public static final class Comment extends Line {
        public static final char HASH = '#';

        private final @NotNull String value;

        public Comment(@NotNull String value) {
            this.value = requireNotNull(value);
        }

        @Contract(pure = true)
        public @NotNull String getValue() {
            return value;
        }

        @Override
        void writeTo(@NotNull Writer writer) throws IOException {
            writer.write(HASH);
            writer.write(' ');
            writer.write(value);
            writeEolTo(writer);
        }

        @Override
        @Contract(value = "null -> false", pure = true)
        public boolean equals(@Nullable Object object) {
            if (this == object) {
                return true;
            }
            if (!(object instanceof Comment)) {
                return false;
            }
            final @NotNull Comment other = (Comment) object;
            return value.equals(other.value);
        }

        @Override
        @Contract(pure = true)
        public int hashCode() {
            return value.hashCode();
        }

        @Override
        @Contract(pure = true)
        public @NotNull String toString() {
            return toStringBuilder(this)
                .property("value", value)
                .build();
        }
    }

    /**
     * Blank {@link Line} in a desktop entry.
     */
    public static final class BlankLine extends Line {
        public static final BlankLine INSTANCE = new BlankLine();

        private BlankLine() {}

        @Override
        void writeTo(@NotNull Writer writer) throws IOException {
            writeEolTo(writer);
        }

        @Override
        @Contract(value = "null -> false", pure = true)
        public boolean equals(@Nullable Object object) {
            return this == object;
        }

        @Override
        @Contract(pure = true)
        public int hashCode() {
            return 0;
        }

        @Override
        @Contract(pure = true)
        public @NotNull String toString() {
            return toStringBuilder(this)
                .build();
        }
    }

    /**
     * Group in a desktop entry.
     */
    public static final class Group {
        public static final char LSB = '[';
        public static final char RSB = ']';

        @Contract(pure = true)
        public static boolean isLegalGroupChar(char c) {
            return Characters.in(c, (char) 32, (char) 126)
                && c != LSB && c != RSB;
        }

        @Contract(pure = true)
        public static boolean isLegalGroupName(@NotNull String string) {
            requireNotNull(string);
            return !string.isEmpty()
                && Strings.all(string, Group::isLegalGroupChar);
        }

        private final @NotNull String name;
        private final @NotNull List<@NotNull Line> lines;
        private final @NotNull Map<@NotNull String, @NotNull Entry> entriesByKey;

        public Group(
            @NotNull String name,
            @NotNull List<@NotNull Line> lines
        ) {
            require(
                isLegalGroupName(name),
                () -> "The string <" + name + "> is an illegal group name"
            );
            requireNotNull(lines);
            this.name = name;
            this.lines = Collections.toList(lines);
            final Map<@NotNull String, @NotNull Entry> entriesByKey = new HashMap<>(lines.size());
            for (final @NotNull Entry entry : Collections.filterIsInstance(lines, Entry.class)) {
                final @NotNull String key = entry.getKey();
                if (entriesByKey.put(key, entry) != null) {
                    throw new IllegalArgumentException("Duplicate entry <" + key + '>');
                }
            }
            this.entriesByKey = unmodifiableMap(entriesByKey);
        }

        @Contract(pure = true)
        public @NotNull String getName() {
            return name;
        }

        @Contract(pure = true)
        public @NotNull List<@NotNull Line> getLines() {
            return lines;
        }

        @Contract(pure = true)
        public @NotNull Map<@NotNull String, @NotNull Entry> getEntriesByKey() {
            return entriesByKey;
        }

        void writeTo(@NotNull Writer writer) throws IOException {
            writer.write(LSB);
            writer.write(name);
            writer.write(RSB);
            writeEolTo(writer);
            for (final @NotNull Line line : lines) {
                line.writeTo(writer);
            }
        }

        @Override
        @Contract(value = "null -> false", pure = true)
        public boolean equals(@Nullable Object object) {
            if (this == object) {
                return true;
            }
            if (!(object instanceof Group)) {
                return false;
            }
            final @NotNull Group other = (Group) object;
            return name.equals(other.name)
                && lines.equals(other.lines);
        }

        @Override
        @Contract(pure = true)
        public int hashCode() {
            return hashCodeBuilder()
                .property(name)
                .property(lines)
                .build();
        }

        @Override
        @Contract(pure = true)
        public @NotNull String toString() {
            return toStringBuilder(this)
                .property("name", name)
                .property("lines", lines)
                .build();
        }
    }

    private static final class ReadingSession {
        private static final Pattern PATTERN_COMMENT = Pattern.compile("^#[ \\t]?(.*)$");
        private static final Pattern PATTERN_GROUP_HEADER = Pattern.compile("^\\[([^\\[\\]]+)]$");
        private static final Pattern PATTERN_ENTRY = Pattern.compile("^([A-Za-z0-9-]+)[ \\t]*=[ \\t]*(.*)$");

        @Contract(value = "_ -> fail", pure = true)
        private static boolean parseIllegalLine(@NotNull String line) throws IOException {
            throw new IOException("The line <" + line + "> is illegal");
        }

        private @NotNull Reader reader;
        private final StringBuilder buffer = new StringBuilder(100);
        private final List<@NotNull Line> currentLines = new ArrayList<>(100);
        private @Nullable String currentGroupName;
        private final List<@NotNull Line> initialLines = new ArrayList<>(10);
        private final List<@NotNull Group> groups = new ArrayList<>(10);

        ReadingSession(@NotNull InputStream input) {
            reader = InputStreams.toReaderUtf8(input);
        }

        // This method is not static so it can use buffer
        private @NotNull String unescape(@NotNull String string) {
            buffer.setLength(0);
            boolean escape = false;
            for (int i = 0; i < string.length(); ++i) {
                char c = string.charAt(i);
                if (escape) {
                    escape = false;
                    switch (c) {
                        case 's':
                            c = ' ';
                            break;
                        case 'n':
                            c = '\n';
                            break;
                        case 't':
                            c = '\t';
                            break;
                        case 'r':
                            c = '\r';
                            break;
                        case '\\':
                            break;
                        default:
                            // Ignore illegal escape
                            // TODO: Should we throw an IllegalArgumentException in this case?
                    }
                    buffer.append(c);
                } else if (c == '\\') {
                    escape = true;
                } else {
                    buffer.append(c);
                }
            }
            return buffer.toString();
        }

        private void addToCurrentLines(@NotNull Line line) {
            currentLines.add(line);
        }

        private void endCurrentLines() throws IOException {
            if (currentGroupName == null) {
                initialLines.addAll(currentLines);
            } else {
                final Group group;
                try {
                    group = new Group(currentGroupName, Collections.toList(currentLines));
                } catch (IllegalArgumentException e) {
                    throw new IOException(e.getMessage());
                }
                groups.add(group);
            }
            currentLines.clear();
        }

        private @Nullable String readLine() throws IOException {
            final @Nullable String line;
            buffer.setLength(0);
            while (true) {
                final int c = reader.read();
                if (c == -1) {
                    if (buffer.length() == 0) {
                        return null;
                    }
                    line = buffer.toString();
                    break;
                }
                if (c == '\n') {
                    line = buffer.toString();
                    break;
                }
                buffer.append((char) c);
            }
            return Strings.trim(line);
        }

        @NotNull DesktopEntry readDesktopEntry() throws IOException {
            while (parseLine());
            try {
                return new DesktopEntry(initialLines, groups);
            } catch (IllegalArgumentException e) {
                throw new IOException(e.getMessage());
            }
        }

        private boolean parseLine() throws IOException {
            final @Nullable String line = readLine();
            if (line == null) {
                endCurrentLines();
                return false;
            }
            return parseBlankLine(line)
                || parseComment(line)
                || parseGroupHeader(line)
                || parseEntry(line)
                || parseIllegalLine(line);
        }

        private boolean parseBlankLine(@NotNull String line) {
            if (!line.isEmpty()) {
                return false;
            }
            addToCurrentLines(BlankLine.INSTANCE);
            return true;
        }

        private boolean parseComment(@NotNull String line) {
            final @NotNull Matcher matcher = PATTERN_COMMENT.matcher(line);
            if (!matcher.matches()) {
                return false;
            }
            addToCurrentLines(new Comment(matcher.group(1)));
            return true;
        }

        private boolean parseGroupHeader(@NotNull String line) throws IOException {
            final @NotNull Matcher matcher = PATTERN_GROUP_HEADER.matcher(line);
            if (!matcher.matches()) {
                return false;
            }
            endCurrentLines();
            currentGroupName = matcher.group(1);
            return true;
        }

        private boolean parseEntry(@NotNull String line) {
            final @NotNull Matcher matcher = PATTERN_ENTRY.matcher(line);
            if (!matcher.matches()) {
                return false;
            }
            addToCurrentLines(new Entry(matcher.group(1), unescape(matcher.group(2))));
            return true;
        }
    }

    public static @NotNull DesktopEntry readFrom(@NotNull InputStream input) throws IOException {
        requireNotNull(input);
        return new ReadingSession(input).readDesktopEntry();
    }

    private static void writeEolTo(@NotNull Writer writer) throws IOException {
        writer.write('\n');
    }

    private final @NotNull List<@NotNull Line> initialLines;
    private final @NotNull List<@NotNull Group> groups;
    private final @NotNull Map<@NotNull String, @NotNull Group> groupsByName;

    public DesktopEntry(
        @NotNull List<@NotNull Line> initialLines,
        @NotNull List<@NotNull Group> groups
    ) {
        requireNotNull(initialLines);
        requireNotNull(groups);
        this.initialLines = Collections.toList(initialLines);
        this.groups = Collections.toList(groups);
        final Map<@NotNull String, @NotNull Group> groupsByName = new HashMap<>(groups.size());
        for (final @NotNull Group group : groups) {
            final @NotNull String name = group.getName();
            if (groupsByName.put(name, group) != null) {
                throw new IllegalArgumentException("Duplicate group <" + name + '>');
            }
        }
        this.groupsByName = unmodifiableMap(groupsByName);
    }

    @Contract(pure = true)
    public @NotNull List<@NotNull Group> getGroups() {
        return groups;
    }

    @Contract(pure = true)
    public @NotNull Map<@NotNull String, @NotNull Group> getGroupsByName() {
        return groupsByName;
    }

    public void writeTo(@NotNull OutputStream output) throws IOException {
        requireNotNull(output);
        final @NotNull Writer writer = OutputStreams.toWriterUtf8(output);
        for (final @NotNull Line line : initialLines) {
            line.writeTo(writer);
        }
        for (final @NotNull Group group : groups) {
            group.writeTo(writer);
        }
        writer.flush();
    }

    @Override
    @Contract(value = "null -> false", pure = true)
    public boolean equals(@Nullable Object object) {
        if (this == object) {
            return true;
        }
        if (!(object instanceof DesktopEntry)) {
            return false;
        }
        final @NotNull DesktopEntry other = (DesktopEntry) object;
        return initialLines.equals(other.initialLines)
            && groups.equals(other.groups);
    }

    @Override
    @Contract(pure = true)
    public int hashCode() {
        return hashCodeBuilder()
            .property(initialLines)
            .property(groups)
            .build();
    }

    @Override
    @Contract(pure = true)
    public @NotNull String toString() {
        return toStringBuilder(this)
            .property("initialLines", initialLines)
            .property("groups", groups)
            .build();
    }
}
