/*
 * Copyright 2023 Smartefact
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

/**
 * Java utilities for the <em>XDG Desktop Entry</em> specification.
 *
 * @author Laurent Pireyn
 * @see <a href="https://specifications.freedesktop.org/desktop-entry-spec/latest/">XDG Desktop Entry Specification</a>
 */
module smartefact.xdg {
    requires smartefact.commons;
    requires static com.google.errorprone.annotations;
    requires static java.compiler;
    requires static org.jetbrains.annotations;

    exports smartefact.xdg.desktopentry;
}
