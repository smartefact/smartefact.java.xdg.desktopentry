/*
 * Copyright 2023 Smartefact
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

package smartefact.xdg.desktopentry;

import java.io.ByteArrayInputStream;
import java.io.ByteArrayOutputStream;
import java.io.IOException;
import java.io.InputStream;
import java.io.OutputStream;
import java.util.List;
import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import static org.junit.jupiter.api.Assertions.assertEquals;
import static org.junit.jupiter.api.Assertions.assertThrows;
import org.junit.jupiter.params.ParameterizedTest;
import org.junit.jupiter.params.provider.Arguments;
import static org.junit.jupiter.params.provider.Arguments.arguments;
import org.junit.jupiter.params.provider.MethodSource;
import org.junit.jupiter.params.provider.ValueSource;

class DesktopEntryTest {
    static @NotNull InputStream openResource(@NotNull String name) {
        final @Nullable InputStream input = DesktopEntryTest.class.getResourceAsStream(name);
        if (input == null) {
            throw new IllegalStateException("Resource <" + name + "> not found");
        }
        return input;
    }

    @ParameterizedTest(name = "{0}")
    @MethodSource("resourcesAndDesktopEntries")
    void readFrom(@NotNull String name, @NotNull DesktopEntry expected) throws IOException {
        final @NotNull DesktopEntry entry;
        try (final InputStream input = openResource(name)) {
            entry = DesktopEntry.readFrom(input);
        }
        assertEquals(expected, entry);
    }

    @ParameterizedTest(name = "{0}")
    @MethodSource("resourcesAndDesktopEntries")
    void writeTo(@NotNull String name, @NotNull DesktopEntry expected) throws IOException {
        final @NotNull DesktopEntry entry1;
        try (final InputStream input = openResource(name)) {
            entry1 = DesktopEntry.readFrom(input);
        }
        final ByteArrayOutputStream arrayOutput = new ByteArrayOutputStream();
        try (final OutputStream output = arrayOutput) {
            entry1.writeTo(output);
        }
        final @NotNull DesktopEntry entry2;
        try (final InputStream input = new ByteArrayInputStream(arrayOutput.toByteArray())) {
            entry2 = DesktopEntry.readFrom(input);
        }
        assertEquals(entry1, entry2);
    }

    static @NotNull Iterable<@NotNull Arguments> resourcesAndDesktopEntries() {
        return List.of(
            arguments(
                "Empty.desktop",
                new DesktopEntry(
                    List.of(),
                    List.of()
                )
            ),
            arguments(
                "Comments.desktop",
                new DesktopEntry(
                    List.of(
                        new DesktopEntry.Comment("comment 1"),
                        new DesktopEntry.Comment("comment 2"),
                        DesktopEntry.BlankLine.INSTANCE
                    ),
                    List.of(
                        new DesktopEntry.Group(
                            "group 1",
                            List.of(
                                new DesktopEntry.Comment("comment 1.1"),
                                new DesktopEntry.Comment("comment 1.2")
                            )
                        )
                    )
                )
            ),
            arguments(
                "Full.desktop",
                new DesktopEntry(
                    List.of(
                        new DesktopEntry.Comment("comment 1"),
                        DesktopEntry.BlankLine.INSTANCE
                    ),
                    List.of(
                        new DesktopEntry.Group(
                            "group 1",
                            List.of(
                                new DesktopEntry.Comment("comment 2"),
                                new DesktopEntry.Entry("key-a", "value-a"),
                                new DesktopEntry.Entry("key-b", "value-b"),
                                DesktopEntry.BlankLine.INSTANCE
                            )
                        ),
                        new DesktopEntry.Group(
                            "group 2",
                            List.of(
                                new DesktopEntry.Entry("key-c", "value-c"),
                                DesktopEntry.BlankLine.INSTANCE,
                                new DesktopEntry.Entry("key-d", "value-d"),
                                new DesktopEntry.Comment("comment 3"),
                                new DesktopEntry.Entry("key-e", " \n\t\r\\")
                            )
                        )
                    )
                )
            )
        );
    }

    @ParameterizedTest(name = "{0}")
    @ValueSource(strings = {
        "IllegalLine1.desktop",
        "IllegalLine2.desktop",
        "DuplicateEntries.desktop",
        "DuplicateGroups.desktop"
    })
    void readFromIllegal(@NotNull String name) throws IOException {
        try (final InputStream input = openResource(name)) {
            assertThrows(IOException.class, () -> DesktopEntry.readFrom(input));
        }
    }
}
